import { TestBed, inject } from '@angular/core/testing';

import { AppVersionService } from './app-version.service';

describe('AppVersionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppVersionService]
    });
  });

  it('should be created', inject([AppVersionService], (service: AppVersionService) => {
    expect(service).toBeTruthy();
  }));
});
