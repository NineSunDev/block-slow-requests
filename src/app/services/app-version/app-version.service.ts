import { Injectable } from '@angular/core';
import { AppVersion } from '../../../environments/app-version';

export interface IAppVersion {
  version: {
    major: number,
    minor: number,
    patch: number,
  },
  status: {
    stage: string,
    number: number,
  },
  build: {
    date: Date,
    number: number,
    total: number,
  },
  commit: string,
}

@Injectable({
  providedIn: 'root'
})
export class AppVersionService {

  public readonly version: IAppVersion = null;

  constructor() {
    let result: any = AppVersion;
    result.build.date = new Date(result.build.date);
    this.version = result;
  }
}
