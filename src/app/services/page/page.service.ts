import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

export interface IPageState {
  title: string,
  displayHeader: boolean,
  displayFooter: boolean,
}

export const PageStateRecord = {
  title: '',
  displayHeader: true,
  displayFooter: true,
};

@Injectable()
export class PageService {

  public state: BehaviorSubject<IPageState> = new BehaviorSubject<IPageState>(PageStateRecord);

  constructor(
    private router: Router,
  ) {
    this.router.events.pipe(
      filter(e => e instanceof NavigationStart),
    ).subscribe(() => {
    const newState = this.state.value;
    newState.displayHeader = true;
    newState.displayFooter = true;
    this.state.next(newState);
    });
  }

  setTitle(title: string) {
    const newState = this.state.value;
    newState.title = title;
    this.state.next(newState);
  }

  showHeader() {
    const newState = this.state.value;
    newState.displayHeader = true;
    this.state.next(newState);
  }

  hideHeader() {
    const newState = this.state.value;
    newState.displayHeader = false;
    this.state.next(newState);
  }

  showFooter() {
    const newState = this.state.value;
    newState.displayFooter = true;
    this.state.next(newState);
  }

  hideFooter() {
    const newState = this.state.value;
    newState.displayFooter = false;
    this.state.next(newState);
  }
}
