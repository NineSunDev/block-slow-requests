import { Injectable } from '@angular/core';
import { AppPackage } from '../../../environments/app-package';

export interface IAppPackage {
  name: string,
  license: string,
  dependencies: any,
  devDependencies: any,
}

@Injectable({
  providedIn: 'root'
})
export class AppPackageService {

  public readonly package: IAppPackage = null;

  constructor() {
    this.package = AppPackage;
  }
}
