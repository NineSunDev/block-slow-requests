import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { Subject } from 'rxjs/internal/Subject';
import { HttpClient } from '@angular/common/http';

export enum EResponseStrategy {
  SERVE_AS_USUAL,
  SERVE_FIRST,
  SERVE_LAST,
}

export enum ERequestType {
  RESOURCE,
  COMMAND,
}

export interface IRequest {
  url: string;
  id?: number;
  type?: ERequestType;
  options?: any;
}

export interface IResponse {
  url: string;
  dismissed: boolean;
  data: any;
}

@Injectable()
export class HttpService {

  private requests: IRequest[];
  public responseStrategy: EResponseStrategy;

  constructor(
    private http: HttpClient,
  ) {
    this.requests = [];
    this.setResponseStrategy(EResponseStrategy.SERVE_FIRST);
  }

  private static sanitizeRequest(_request): IRequest {
    return {
      url: _request.url + '?' + Math.random() * Math.random(),
      type: _request.type ? _request.type : ERequestType.RESOURCE,
      options: _request.options ? _request.options : void 0,
    };
  }

  public setResponseStrategy(strategy: EResponseStrategy) {
    this.responseStrategy = strategy;
  }

  public get(url: string, options: any, type: ERequestType = ERequestType.RESOURCE): Observable<any> {
    const request: IRequest = HttpService.sanitizeRequest({ url, type, options });
    console.log(request);

    switch (this.responseStrategy) {
      case EResponseStrategy.SERVE_FIRST:
        return this.getServeFirst(request);

      case EResponseStrategy.SERVE_LAST:
        return this.getServeLast(request);

      default:
      case EResponseStrategy.SERVE_AS_USUAL:
        return this.getAsUsual(request);
    }
  }

  private getAsUsual(request: IRequest): Observable<any> {
    const sbj: any = new Subject();

    console.log('AU:', request);

    this.http.get(request.url, request.options).subscribe(
      (_response) => {
        sbj.next({
          url: request.url,
          dismissed: false,
          data: null,
        });
      },
      (error) => {
        sbj.next({
          url: request.url,
          dismissed: false,
          data: error,
        });
      });

    return sbj;
  }

  getServeFirst(request: IRequest): Observable<any> {
    const sbj: any = new Subject();
    request.id = this.requests.length;
    this.requests.push(request);

    console.log('SF:', request);

    this.http.get(request.url, request.options).subscribe(
      (_response) => {
        const response: IResponse = {
          url: request.url,
          dismissed: false,
          data: null,
        };

        // Remove all but the current request from queue
        this.requests = this.requests.filter((req) => {
          console.log(req.id === request.id, req.type !== request.type);
          if (req.id === request.id) {
            return true;
          }

          return req.type !== request.type;
        });

        // Dismiss current request if not in queue
        response.dismissed = this.requests.indexOf(request) === -1;

        sbj.next(response);
      },
      (error) => {
        const response: IResponse = {
          url: request.url,
          dismissed: false,
          data: error,
        };

        // Remove all but the current request from queue
        this.requests = this.requests.filter((req) => {
          console.log(req.id === request.id, req.type !== request.type);
          if (req.id === request.id) {
            return true;
          }

          return req.type !== request.type;
        });

        // Dismiss current request if not in queue
        response.dismissed = this.requests.indexOf(request) === -1;

        sbj.next(response);
      });

    return sbj;
  }

  getServeLast(request: IRequest): Observable<any> {
    const sbj: any = new Subject();
    request.id = this.requests.length;
    this.requests.push(request);

    console.log('SL:', request);

    this.http.get(request.url, request.options).subscribe(
      (_response) => {
        const response: IResponse = {
          url: request.url,
          dismissed: false,
          data: null,
        };

        // Only leave last request in queue
        this.requests = [this.requests[this.requests.length - 1]];

        // Dismiss current request if not in queue
        response.dismissed = this.requests.indexOf(request) === -1;

        sbj.next(response);
      },
      (error) => {
        const response: IResponse = {
          url: request.url,
          dismissed: false,
          data: error,
        };

        // Only leave last request in queue
        this.requests = [this.requests[this.requests.length - 1]];

        // Dismiss current request if not in queue
        response.dismissed = this.requests.indexOf(request) === -1;

        sbj.next(response);
      });

    return sbj;
  }
}
