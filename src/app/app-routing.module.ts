import { NgModule } from '@angular/core';

import {
  RouterModule,
  Routes,
} from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'index',
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'index',
    component: IndexComponent
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      useHash: true,
      enableTracing: false,
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      urlUpdateStrategy: 'eager',
    }),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule {
}
