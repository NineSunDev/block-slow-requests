import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './components/app/app.component';
import { MatCardModule } from '@angular/material';
import { ContactComponent } from './pages/contact/contact.component';
import { IndexComponent } from './pages/index/index.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { PageService } from './services/page/page.service';
import { AppRoutingModule } from './app-routing.module';
import { AppVersionService } from './services/app-version/app-version.service';
import { AppPackageService } from './services/app-package/app-package.service';
import { HttpService } from './services/http/http.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    IndexComponent,
    NotFoundComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    // Angular Material
    MatCardModule,
    // Default Modules
    BrowserModule,
    HttpClientModule,
    //Custom Modules
    AppRoutingModule,
  ],
  providers: [
    PageService,
    AppVersionService,
    AppPackageService,
    HttpService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
