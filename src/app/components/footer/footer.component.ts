import { Component, OnInit } from '@angular/core';
import { AppVersionService, IAppVersion } from '../../services/app-version/app-version.service';
import { AppPackageService, IAppPackage } from '../../services/app-package/app-package.service';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public version: IAppVersion = null;
  public package: IAppPackage = null;

  constructor(
    private versionService: AppVersionService,
    private packageService: AppPackageService,
  ) {
    this.version = this.versionService.version;
    this.package = this.packageService.package;
  }

  ngOnInit() {
  }

}
