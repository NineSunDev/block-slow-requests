import { Component, OnInit } from '@angular/core';
import { IPageState, PageService } from '../../services/page/page.service';
import { AppPackageService } from '../../services/app-package/app-package.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public shrinkHeader = false;
  public pageState: IPageState = null;


  constructor(
    private pageService: PageService,
    private packageService: AppPackageService,
  ) { }

  ngOnInit() {
    this.pageService.state.subscribe((state) => {
      if (this.pageState !== state) this.onDataChange(state);
    })
  }

  public onScroll(event: MouseEvent): void {
    const target = event.target as HTMLElement;
    this.shrinkHeader = target.scrollTop > 150;
  }

  private onDataChange(state: IPageState): void {
    this.pageState = Object.assign({}, state);
    document.title = state.title + ' - ' + this.packageService.package.name;
  }
}
