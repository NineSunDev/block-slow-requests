import { Component, OnInit } from '@angular/core';
import { PageService } from '../../services/page/page.service';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor(
    private pageService: PageService
  ) { }

  ngOnInit() {
    this.pageService.setTitle('Contact');
  }

}
