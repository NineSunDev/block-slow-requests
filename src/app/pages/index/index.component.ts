import { Component, OnInit } from '@angular/core';
import { PageService } from '../../services/page/page.service';
import { ERequestType, EResponseStrategy, HttpService, IResponse } from '../../services/http/http.service';

@Component({
  selector: 'index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {

  public output = '- Nothing loaded yet -';

  public strategy: EResponseStrategy;

  public resourceOneStatus = '';
  public resourceTwoStatus = '';
  public resourceThreeStatus = '';

  public commandOneStatus = '';
  public commandTwoStatus = '';
  public commandThreeStatus = '';

  private url1sec = 'https://api.bitbucket.org/2.0/repositories/ninesundev';
  private url3sec = 'http://slowwly.robertomurray.co.uk/delay/3000/url/https://api.bitbucket.org/2.0/repositories/ninesundev';
  private url6sec = 'http://slowwly.robertomurray.co.uk/delay/6000/url/https://api.bitbucket.org/2.0/repositories/ninesundev';

  constructor(
    private pageService: PageService,
    private http: HttpService,
  ) {
    this.strategy = http.responseStrategy;
  }

  ngOnInit() {
    this.pageService.setTitle('Home');
  }

  setStrategy(strategy: EResponseStrategy) {
    this.strategy = strategy;
    this.http.setResponseStrategy(strategy);
  }

  loadTextOne() {
    this.output = 'loading';
    this.resourceOneStatus = 'loading';

    this.http.get(this.url6sec, null, ERequestType.RESOURCE)
      .subscribe((response) => {
        this.resourceOneStatus = response.dismissed ? 'discarded' : 'displayed';
        if (!response.dismissed) {
          this.output = JSON.stringify({ RESOURCE_1: response }, null, 2);
        }
      });
  }
  loadTextTwo() {
    this.output = 'loading';
    this.resourceTwoStatus = 'loading';

    this.http.get(this.url3sec, null, ERequestType.RESOURCE)
      .subscribe((response: IResponse) => {
        this.resourceTwoStatus = response.dismissed ? 'discarded' : 'displayed';
        if (!response.dismissed) {
          this.output = JSON.stringify({ RESOURCE_2: response }, null, 2);
        }
      });
  }
  loadTextThree() {
    this.output = 'loading';
    this.resourceThreeStatus = 'loading';

    this.http.get(this.url1sec, null, ERequestType.RESOURCE)
      .subscribe((response: IResponse) => {
        this.resourceThreeStatus = response.dismissed ? 'discarded' : 'displayed';
        if (!response.dismissed) {
          this.output = JSON.stringify({ RESOURCE_3: response }, null, 2);
        }
      });
  }

  sendCommandOne() {
    this.output = 'loading';
    this.commandOneStatus = 'loading';

    this.http.get(this.url6sec, null, ERequestType.COMMAND)
      .subscribe((response: IResponse) => {
        this.commandOneStatus = response.dismissed ? 'discarded' : 'displayed';
        if (!response.dismissed) {
          this.output = JSON.stringify({ COMMAND_1: response }, null, 2);
        }
      });
  }
  sendCommandTwo() {
    this.output = 'loading';
    this.commandTwoStatus = 'loading';

    this.http.get(this.url3sec, null, ERequestType.COMMAND)
      .subscribe((response: IResponse) => {
        this.commandTwoStatus = response.dismissed ? 'discarded' : 'displayed';
        if (!response.dismissed) {
          this.output = JSON.stringify({ COMMAND_2: response }, null, 2);
        }
      });
  }
  sendCommandThree() {
    this.output = 'loading';
    this.commandThreeStatus = 'loading';

    this.http.get(this.url1sec, null, ERequestType.COMMAND)
      .subscribe((response: IResponse) => {
        this.commandThreeStatus = response.dismissed ? 'discarded' : 'displayed';
        if (!response.dismissed) {
          this.output = JSON.stringify({ COMMAND_3: response }, null, 2);
        }
      });
  }
}
