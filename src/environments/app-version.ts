export const AppVersion = {
  "version": {
    "major": 0,
    "minor": 0,
    "patch": 1
  },
  "status": {
    "stage": null,
    "number": 0
  },
  "build": {
    "date": "Thu Oct 11 2018 10:20:26 GMT+0200 (CEST)",
    "number": 4,
    "total": 24
  },
  "commit": null
};