# TAW

A lightweight boilerplate to get started with TypeScript, Angular5 and Webpack

This Boilerplate ships with appversion by default. It is configured in such a way,
that every rebuild increments the build counter for the current version. To bump
a version read below at _Commands_.

This also ships with default pages for index, contact and not-found (404)

If you choose not to use appversion, just delete prebuild/, gulpfile.js and the respecting
scripts in the package.json

The package.json-file and appversion.json-file are accessible through services 
inside your app. Take a look at the footer.component for and example.
See prebuild/prebuild.js for which fields get injected

Protractor and Karma test are available but just in a very basic form.

Happy coding. 

### Setup
##### Prerequisites
* node.js
* npm
* gulp
* angular/cli@^6.1.4
* appversion@^1.7.1

_Of course these have to be installed globally_


#### Installing
* Get the Repository: `git clone git@bitbucket.org:NineSunDev/taw-base.git`
* Run `npm i` to install it
* Run `npm start` to serve it with watching your sources

#### Recommended Folder-Structure
``` 
    /
    |_ build/
    |_ e2e/
    |_ node_modules/
    |_ src/
     |_ app/                   
      |_ components/
        |_ app
         |_ app.component.html
         |_ app.component.scss
         |_ app.component.ts
         |_ app.component.spec.ts
        |_ ...
      |_ pages/
       |_ index/
        |_ index.compnent.html
        |_ index.compnent.scss
        |_ index.compnent.ts
        |_ index.compnent.spec.ts
       |_ ...
      |_ services/
       |_ page.service.spec.ts
       |_ page.service.ts
     |_ app.module.ts
     |_ app-routing.module.ts
    |_ assets/
    |_ environments/
    |_ prebuild/
    |_ stylesheets/
    |_ angular.json
    |_ appversion.json
    |_ gulpfile.js
    |_ karma.conf.js
    |_ package.json
    |_ package-lock.json
    |_ protractor.conf.js
    |_ README.md
    |_ tsconfig.json
    |_ tslint.json
```

### Commands
* Production Build: `npm build`
* Serve: `npm start`
* Testing: `npm test`
* Increment Major Version: `npm run major`
* Increment Minor Version: `npm run minor`
* Increment Patch Version: `npm run patch`
* Code Generation: 
    
    * Modules: `ng g m modules/{{module-name}}`
    * Components: `ng g c components/{{component-name}}`
    * Pages: `ng g c pages/{{page-name}}`
    * Services: `ng g s services/{{service-name}}`
    * Directives: `ng g d directives/{{directive-name}}`
    * Pipes: `ng g p pipes/{{pipe-name}}`
    * ...

### Owner
* [Nico Sänger](nico.saenger@gmail.com) (Nico Sänger)

### License
[![cc-by](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)
